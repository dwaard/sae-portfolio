# ZELFBEOORDELING FASE 1: ORIËNTATIEFASE #

## Kans 1 ##
### Situatie ###
In de cursus... 

### Taak ###
Mijn taak was het om ...

### Activiteiten ###
Om deze taak uit te voeren heb ik ...

### Resultaten ###
De uitkomst was een ..., zie document XYZ Dit was beoordeeld met een GO (zie feedback formulier..), met als feedback ...

### Reflectie ###
Ik was tevreden met het resultaat om ... De feedback is te verklaren doordat ik wel/niet... 

### Transfer ###
De volgende keer als ik een vergelijkbare taak uit moet voeren zal ik ...